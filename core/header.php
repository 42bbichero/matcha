<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <!-- <meta name="viewport" content="initial-scale=1.0, user-scalable=no" /> -->
        <meta name="viewport" content="width=device-width" />
        <!-- CSS includes -->
        <link rel="stylesheet" type="text/css" href="<?php echo "$url_path/css/animate.css"; ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo "$url_path/css/styles.css"; ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo "$url_path/css/main_page.css"; ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo "$url_path/css/omnibar.css"; ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo "$url_path/css/messages.css"; ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo "$url_path/css/user_settings.css"; ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo "$url_path/css/tags.css"; ?>" />
        <link rel="stylesheet" href="https://unpkg.com/purecss@0.6.1/build/pure-min.css" />

        <!-- JS bundle include -->
        <script src="<?php echo "$url_path/js-build/bundle.js"; ?>"></script>
        <script src="<?php echo "$url_path/node_modules/socket.io-client/dist/socket.io.js"; ?>"></script>
        <!-- <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script> -->
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> -->


        <title>Matcha</title>
    </head>
