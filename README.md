# For deployment
### In matcha/core/ create config.php file like :
```
<?hh

$url_path = "";

//Option for log request : SW -> log request SHOW, SELECT and INSERT INTO, UPDATE  RW-> Only INSERT INTO, UPDATE
$log_request = "RW";

//Show all request 1 -> show all  0 -> show nothing
$debug_req = 0;

// Config for connecting to the DB
$host_name = "127.0.0.1";
$db_name = "<db-name>";
$port = 3306;
$username = "<username>";
$password = "<password>";
$option_pdo = array(
    // Activation PDO exceptions:
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_TIMEOUT => 10
);
```
### Matcha SQL structure is in bdd.sql file

# For developement
# Project : Matcha

# Rules :

## Commit :

- Each time you finished your work, you must push
- Each commit must be comments correctly, not like (New feature)
- Get familiar with "git add -p" (or -i) to split up changes into logical commits
- Don't commit files that ohter work on
- Each files you commit must be test BEFORE PUSH
- Never use -m message, use only 'git commit' and after config your code editor
all your commit message will be write on it

## Variable

- Allways give a correct name to variables (No like '$req1' or '$tmp')
- When you execute a request for print a table for example, your variable name
must start with 'sqlq' (Ex: $sqlq_user_info)
- When you stock a fetched result query in a variable, his name must started
with 'sqlr' (Ex: $sqlr_nb_result)

A good example :

$sqlq_items = $PDO->query("SELECT * FROM items");

while ($sqlr_item = $sqlq_items->fetch(PDO::FETCH_ASSOC))
{
    // Print item
}

After an example like this you must clean variables, after each :
- Requests
- Requests fetched
- Requests parameters

Use 'unset($var_name)' to process this clean

## Database

- Never delete, rename, resize a column, table before process a GREP in all file
- Consult every members before remove a column or table, this can be very critic.
All column must be typed and size correctly, Ex:
- A number must always be an INT(11)
- A basic column must be set at VARCHAR(100) (Size must be change following situations)
- All dates wihtout minutes and seconds must be set at DATE
- Dates with minutes and seconds must be set at TIMESTAMP

ALLWAYS log out when finished task on adminer.

# IF FUCKING PROBLEMEEEMEE DONT WORK
export PATH=$PATH:$(npm config get prefix)/bin
