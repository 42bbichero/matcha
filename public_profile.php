<?hh

$page_name = "public_profile";

// require mandatody page
require("core/ini.php");

if (isset($_GET['profile_username']))
{
    $public_user = htmlentities(addslashes($_GET['profile_username']));
    if (!empty($check_public_user = $PDO->query("SELECT username FROM users WHERE username = '$public_user'")->fetchColumn()))
        $valid_user = 1;
    else
        $valid_user = 0;
}

// Create user infomation div in top of public prpfile page.
class :Public_profile extends :x:element
{
    use XHPHelpers;
    use XHPReact;

    attribute
    :xhp:html-element,
    array item_edit_list @required,
    string url_path @required,
    string resources_path @required,
    string src_username @required,
    string dst_username @required,
    string id_src_username @required,
    string id_dst_username @required,
    string percent_xp @required,
    string xp @required,
    string level @required,
    string prez @required,
    string src_prez @required,
    string alt1 @required,
    string alt2 @required,
    string city @required,
    string lat @required,
    string lon @required,
    string id @required,
    string ls @required,
    string active @required,
    string blocked_by @required,
    bool   status @required,
    bool   matchs @required,
    array  tags @required;

    protected function render(): XHPRoot
    {
        $this->constructReactInstance(
        'Public_profile',
            Map {
                'item_edit_list' => $this->:item_edit_list,
                'url_path' => $this->:url_path,
                'resources_path' => $this->:resources_path,
                'src_username' => $this->:src_username,
                'dst_username' => $this->:dst_username,
                'id_src_username' => $this->:id_src_username,
                'id_dst_username' => $this->:id_dst_username,
                'percent_xp' => $this->:percent_xp,
                'xp' => $this->:xp,
                'level' => $this->:level,
                'prez' => $this->:prez,
                'src_prez' => $this->:src_prez,
                'alt1' => $this->:alt1,
                'alt2' => $this->:alt2,
                'city' => $this->:city,
                'lat' => $this->:lat,
                'lon' => $this->:lon,
                'tags' => $this->:tags,
                'id' => $this->:id,
                'ls' => $this->:ls,
                'active' => $this->:active,
                'blocked_by' => $this->:blocked_by,
                'status' => $this->:status,
                'matchs' => $this->:matchs,
            }
        );
        return <div id={$this->getID()} />;
    }
}

// Get back all near profiles / users
$sql_user_info =
"SELECT users.id, username, age, bio, id_tags, percent_xp, xp, level, gender, orientation, email, phone, prez, alt1, alt2, city, lon, lat, last_signin_date
FROM users
LEFT OUTER JOIN profile_stats ON users.id = profile_stats.id_user
WHERE username = '" . $public_user . "'";

$sql_current_tags = "SELECT id_tags FROM users WHERE username = '" . $_SESSION['auth']['username'] . "'";

$user_infos = $PDO->query($sql_user_info)->fetch(PDO::FETCH_ASSOC);
$current_tags = $PDO->query($sql_current_tags)->fetchColumn();

$current_tags = explode(',', $current_tags);
$cmp_tags     = explode(',', $user_infos['id_tags']);
$tmp_arr = array_intersect($cmp_tags, $current_tags);
$tmp_arr = implode(',', $tmp_arr);

if ($tags = $PDO->query("SELECT id, name FROM tags")->fetchAll(PDO::FETCH_ASSOC)); else $tags = null;;
if ($tmp_arr) {
    if ($tags = $PDO->query("SELECT name FROM tags WHERE id IN ($tmp_arr)")->fetchAll(PDO::FETCH_ASSOC));
    else $tags = null;
}
else $tags = null;


$id_src     = $PDO->query("SELECT users.id FROM users WHERE username = '" . $_SESSION['auth']['username'] . "'")->fetchColumn();
$tmp_status = $PDO->query("SELECT id_liked FROM users WHERE username = '$public_user' ")->fetchColumn();
$tmp_status = explode(',', $tmp_status);

if (in_array($id_src, $tmp_status))
    $liked = true;
else
    $liked = false;

$id_src     = $PDO->query("SELECT users.id  FROM users WHERE username = '" . $_SESSION['auth']['username'] . "'")->fetchColumn();
$tmp_matchs = $PDO->query("SELECT id_matchs FROM users WHERE username = '$public_user' ")->fetchColumn();
$tmp_matchs = explode(',', $tmp_matchs);
$active     = $PDO->query("SELECT active  FROM users WHERE username = '" . $_SESSION['auth']['username'] . "'")->fetchColumn();

if (in_array($id_src, $tmp_matchs))
    $matchs = true;
else
    $matchs = false;
$src_prez = $PDO->query("SELECT prez FROM users WHERE username = '" . $_SESSION['auth']['username'] . "'")->fetchColumn();

// Get the list of users who blocks the dst user.
$sqlr_blocked_by = $PDO->query("SELECT blocked_by FROM users WHERE username = '" . $_SESSION['auth']['username'] . "'")->fetchColumn();

// Print user information on top of page (xp bar, image profile, etc ...)

if ($valid_user == 1)
{
    $public_profile =
    <body>
        <div>
            <x:js-scope>
                <Public_profile
                        item_edit_list={
                                array(
                                        array("name" => "gender", "value" => $user_infos['gender']),
                                        array("name" => "orientation", "value" => $user_infos['orientation']),
                                        array("name" => "bio", "value" => $user_infos['bio']),
                                        array("name" => "age", "value" => $user_infos['age']),
                                        array("name" => "email", "value" => $user_infos['email']),
                                        array("name" => "phone", "value" => $user_infos['phone']),
                                        array("name" => "city", "value" => $user_infos['city']))}
                                resources_path={"resources"}
                                url_path={$url_path}
                                id={$user_infos['id']}
                                src_username={$_SESSION['auth']['username']}
                                dst_username={$user_infos['username']}
                                id_src_username={$id_src}
                                id_dst_username={$user_infos['id']}
                                percent_xp={$user_infos['percent_xp']}
                                xp={$user_infos['xp']}
                                level={$user_infos['level']}
                                prez={$user_infos['prez']}
                                src_prez={$src_prez}
                                alt1={$user_infos['alt1']}
                                alt2={$user_infos['alt2']}
                                city={$user_infos['city']}
                                lat={$user_infos['lat']}
                                lon={$user_infos['lon']}
                                ls={$user_infos['last_signin_date']}
                                tags={$tags}
                                status={$liked}
                                matchs={$matchs}
                                active={$active}
                                blocked_by={$sqlr_blocked_by}
                            />
            </x:js-scope>
        </div>
    </body>;

    echo $public_profile . $html_closure;
}
