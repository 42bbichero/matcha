<?hh

$page_name = "post";
$output = false;

// require(__DIR__ . "/vendor/autoload.php");
// require(__DIR__ . "/vendor/facebook/xhp-lib/init.php");
// require(__DIR__ . "/class/hack/MyQuery.php");
// require(__DIR__ . "/class/hack/MyPool.php");
// require(__DIR__ . "/core/config.php");
// require(__DIR__ . "/class/hack/Connection.class.php");
// $PDO = new Connection($host_name, $db_name, $username, $password, $log_request, $debug_req, $option_pdo);

// Include core file without header
require("core/ini.php");

if (isset($_POST['send_message']) && !empty($_POST['send_message']))
{
    $dst_username = addslashes($_POST['dst_username']);
    $src_username = addslashes($_POST['src_username']);
    $message = addslashes($_POST['send_message']);
    // var_dump($message);
    // Check validy of users
    if ($id_user_src = $PDO->query("SELECT id FROM users WHERE username = '$src_username'")->fetchColumn())
    {
        if ($id_user_dst = $PDO->query("SELECT id FROM users WHERE username = '$dst_username'")->fetchColumn())
        {
            $id_message = $PDO->req_last_insert("query",
                "INSERT INTO messages (id_user_src, id_user_dst, body, status, active, creation_date)
                VALUES ('$id_user_src', '$id_user_dst', '$message', 'F', 'Y', NOW())");

            $PDO->query("UPDATE profile_stats SET xp = xp + 50  WHERE id_user = $id_user_src");
            $PDO->query("UPDATE profile_stats SET xp = xp + 100 WHERE id_user = $id_user_dst");

            $dst_prez = $PDO->query("SELECT prez FROM users WHERE username = '$src_username'")->fetchColumn();

            $message_obj = array('active' => 'Y', 'id' => $id_message, 'body' => stripslashes($message), 'seen' => 'N', 'dst_username' => $dst_username, 'onMouse' => false, 'src_username' => $src_username, 'prez' => $dst_prez);
            echo json_encode($message_obj);
        }
        else
            $PDO->log_errors("Bad dst user : $dst_username", "false");
    }
    else
        $PDO->log_errors("Bad src user : $src_username", "false");
}

if (isset($_POST['open_message']) && !empty($_POST['open_message']))
{
    $dst_username = addslashes($_POST['dst_username']);
    $src_username = addslashes($_POST['src_username']);
    $message = addslashes($_POST['open_message']);
    // var_dump($message);
    // Check validy of users
    if ($id_user_src = $PDO->query("SELECT id FROM users WHERE username = '$src_username'")->fetchColumn())
    {
        if ($id_user_dst = $PDO->query("SELECT id FROM users WHERE username = '$dst_username'")->fetchColumn())
        {
            if (empty($PDO->query("SELECT id FROM messages WHERE active = 'Y' AND (id_user_src = '$id_user_src' AND id_user_dst = '$id_user_dst') OR (id_user_src = '$id_user_dst' AND id_user_dst = '$id_user_src')")->fetchAll())) {
                $id_message = $PDO->req_last_insert("query",
                    "INSERT INTO messages (id_user_src, id_user_dst, body, status, active, creation_date)
                    VALUES ('$id_user_src', '$id_user_dst', '$message', 'F', 'Y', NOW())");

                $PDO->query("UPDATE profile_stats SET xp = xp + 50  WHERE id_user = $id_user_src");
                $PDO->query("UPDATE profile_stats SET xp = xp + 100 WHERE id_user = $id_user_dst");

                $dst_prez = $PDO->query("SELECT prez FROM users WHERE username = '$src_username'")->fetchColumn();

                $message_obj = array('active' => 'Y', 'id' => $id_message, 'body' => stripslashes($message), 'seen' => 'N', 'dst_username' => $dst_username, 'onMouse' => false, 'src_username' => $src_username, 'prez' => $dst_prez);
                echo json_encode($message_obj);
            }
            else
                echo "started";
        }
        else
            $PDO->log_errors("Bad dst user : $dst_username", "false");
    }
    else
        $PDO->log_errors("Bad src user : $src_username", "false");
}

else if (isset($_POST['delete_message']))
{
    $id_del_message = (int)addslashes($_POST['delete_message']);
    if ($_POST['src_username'] === $_SESSION['auth']['username'])
    {
        if (is_numeric($_POST['delete_message']) && (int)$_POST['delete_message'] > 0)
        {
            if ($PDO->query("UPDATE messages SET active = 'N' WHERE id = $id_del_message"))
                echo json_encode(array('id_del_message' => $id_del_message));
            else
                $PDO->log_errors("Can't delete message with id : $id_del_message", "false");
        }
        else
            $PDO->log_errors("Bad id message : $id_del_message", "false");
    }
    else
        $PDO->log_errors("You can only delete your own message", "false");
}

else if (isset($_POST['data_type']) && !empty($_POST['data_type']))
{
    if (in_array($_POST['data_type'], array("first_name", "last_name", "age", "email", "phone", "bio", "gender", "orientation", "city")))
    {
        $value = htmlentities(addslashes($_POST['value']));
        $id_user = $PDO->getIdUser();
        if ($PDO->query("UPDATE users SET " . $_POST['data_type'] . " = '$value' WHERE id = $id_user"))
            echo json_encode(array("data_type" => $_POST['data_type'], "value" => stripslashes($value)));
        else
            $PDO->log_errors("Error in request when modify " . $_POST['data_type'], "false");
    }
    else
        $PDO->log_errors("Error of data type when editing field in setting", "false");
}

else if (isset($_POST['longitude']) && isset($_POST['latitude']))
{
    //if (is_float($_POST['longitude']) && is_float($_POST['latitude']))
    {
        $json = get_json("http://maps.googleapis.com/maps/api/geocode/json?latlng=" . $_POST['latitude'] . "," . $_POST['longitude'] . "&sensor=true");
        $infos = json_decode($json, true);
        $id_user = $PDO->getIdUser();
        if ($PDO->query("UPDATE users
                        SET city = '" . $infos['results'][0]['address_components'][2]['long_name'] . "',
                         lon = '" . $_POST['longitude'] . "', position_state = 'A' , lat = '" . $_POST['latitude'] . "' WHERE id = $id_user"))
        {
            echo json_encode(array("value" => $infos['results'][0]['address_components'][2]['long_name']));
        }
    }
}

else if (isset($_POST['type']) && $_POST['type'] === "add_tag")
{

    if (isset($_POST['username']) && !empty($_POST['username']) && isset($_POST['tag']) && !empty($_POST['tag']))
    {
        $tag = addslashes($_POST['tag']);
        $username = addslashes($_POST['username']);
        $id_user_src = $PDO->query("SELECT id FROM users WHERE username = '$username'")->fetchColumn();

        if (empty($id_tag = $PDO->query("SELECT id FROM tags WHERE name = '$tag'")->fetchColumn()))
        {
            $id_tag = $PDO->req_last_insert("query", "INSERT INTO tags (name, creation_date) VALUES ('$tag', NOW())");
            $PDO->query("UPDATE profile_stats SET xp = xp + 50  WHERE id = '$id_user_src'");
        }
        $user_id_tag = $PDO->query("SELECT id_tags FROM users WHERE username = '$username'")->fetchColumn();
        if (in_array($id_tag, explode(',', $user_id_tag)) === FALSE)
        {
            if (!empty($user_id_tag))
                $user_id_tag .= ',';
            $user_id_tag .= $id_tag;
        }
        if ($PDO->query("UPDATE users SET id_tags = '$user_id_tag' WHERE username = '$username'"))
        {
            $PDO->query("UPDATE profile_stats SET xp = xp + 10  WHERE id_user = '$id_user_src'");
            echo "type=add_tag&username=$username&tag=$tag";
        }
        else
        {
            $PDO->log_errors("Error in request when updating the database", "false");
            echo false;
        }
    }
    else
        $PDO->log_errors("Post request for adding tag with not username (" . $_POST['username'] . ") or no tag (" . $_POST['tag'] . ")", "false");
}

else if (isset($_POST['type']) && $_POST['type'] === "del_tag")
{
    if (isset($_POST['username']) && !empty($_POST['username']) && isset($_POST['tag']) && !empty($_POST['tag']))
    {
        $tag = addslashes($_POST['tag']);
        $username = addslashes($_POST['username']);
        $id_tag = $PDO->query("SELECT id FROM tags WHERE name = '$tag'")->fetchColumn();
        $user_id_tag = $PDO->query("SELECT id_tags FROM users WHERE username = '$username'")->fetchColumn();
        $tmp_tag = explode(',', $user_id_tag);
        $tmp_ret = array_diff($tmp_tag, [$id_tag]);
        $user_id_tag = implode(',', $tmp_ret);
        if ($PDO->query("UPDATE users SET id_tags = '$user_id_tag' WHERE username = '$username'"))
            echo "type=del_tag&username=$username&tag=$tag";
        else
        {
            $PDO->log_errors("Error in request when updating the database", "false");
            echo false;
        }
    }
    else
        $PDO->log_errors("Post request for delete tag with no username (" . $_POST['username'] . ") or no tag (" . $_POST['tag'] . ")", "false");
}

else if (isset($_POST['type']) && $_POST['type'] === "set_block")
{
    if (isset($_POST['username']))
    {
        $username = addslashes($_POST['username']);
        $user_src = $_SESSION['auth']['username'];
        $id_src   = $PDO->query("SELECT users.id FROM users WHERE username = '$user_src'")->fetchColumn();

        $user_id_blocked = $PDO->query("SELECT blocked_by FROM users WHERE username = '$username'")->fetchColumn();
        if (!empty($user_id_blocked))
            $user_id_blocked .= ',';
        $tmp_list = explode(',', $user_id_blocked);
        if (array_search($id_src, $tmp_list) === false)
        {
            $user_id_blocked .= $id_src;
            if ($PDO->query("UPDATE users SET blocked_by = '$user_id_blocked' WHERE username = '$username'"))
                echo "type=set_block&username=$username";
            else
            {
                $PDO->log_errors("Error in request when updating the database", "false");
                echo "type=set_block&state=false";
            }
        }
        else
        {
            $PDO->log_errors("User already blocked", "false");
            echo "already_blocked";
        }
    }
    else
        $PDO->log_errors("Post request for block user with no username (" . $_POST['username'] . ")", "false");
}

else if (isset($_POST['type']) && $_POST['type'] === "set_report")
{
    if (isset($_POST['username']))
    {
        $username = addslashes($_POST['username']);
        $user_src = $_SESSION['auth']['username'];
        $id_src   = $PDO->query("SELECT users.id FROM users WHERE username = '$user_src'")->fetchColumn();

        $user_id_reported = $PDO->query("SELECT reported_by FROM users WHERE username = '$username'")->fetchColumn();
        if (!empty($user_id_reported))
            $user_id_reported .= ',';
        $tmp_list = explode(',', $user_id_reported);
        if (array_search($id_src, $tmp_list) === false)
        {
            $user_id_reported .= $id_src;
            if ($PDO->query("UPDATE users SET reported_by = '$user_id_reported' WHERE username = '$username'"))
                echo "type=set_report&username=$username";
            else
            {
                $PDO->log_errors("Error in request when updating the database", "false");
                echo "type=set_report&state=false";
            }
        }
        else
        {
            $PDO->log_errors("User already reported", "false");
            echo 0;
        }
    }
    else
        $PDO->log_errors("Post request for report user with no username (" . $_POST['username'] . ")", "false");
}

else if (isset($_POST['type']) && $_POST['type'] === "relation_state")
{
    $user_src = addslashes($_SESSION['auth']['username']);
    $id_src   = $PDO->query("SELECT users.id FROM users WHERE username = '$user_src'")->fetchColumn();
    $id_liked = (int)$_POST['id_liked'];

    $list_id_liked = $PDO->query("SELECT id_liked FROM users WHERE username = '$user_src'")->fetchColumn();
    if ($_POST['state'] === "like")
    {
        if (!empty($list_id_liked))
            $list_id_liked .= ',';
        $tmp_list = explode(',', $list_id_liked);
        if (array_search($id_liked, $tmp_list) === false)
        {
            $list_id_liked .= $id_liked;
            if ($PDO->query("UPDATE users SET id_liked = '$list_id_liked' WHERE username = '$user_src'"))
            {
                $PDO->query("UPDATE profile_stats SET xp = xp + 200  WHERE id_user = '$id_src'");
                $PDO->query("UPDATE profile_stats SET xp = xp + 300  WHERE id_user = '$id_liked'");

                if (!empty($match = $PDO->query("SELECT id_liked FROM users WHERE users.id = '$id_liked'")->fetchColumn()))
                {
                    $tmp_match = explode(',', $match);
                    if (in_array($id_src, $tmp_match))
                    {
                        $container_src = $PDO->query("SELECT id_matchs FROM users WHERE users.id = '$id_src'")->fetchColumn();
                        $container_dst = $PDO->query("SELECT id_matchs FROM users WHERE users.id = '$id_liked'")->fetchColumn();
                        if (!empty($container_src))
                            $container_src .= ',';
                        $container_src .= $id_liked;
                        if (!empty($container_dst))
                            $container_dst .= ',';
                        $container_dst .= $id_src;
                        $PDO->query("UPDATE users SET id_matchs = '$container_src' WHERE users.id = '$id_src'");
                        $PDO->query("UPDATE users SET id_matchs = '$container_dst' WHERE users.id = '$id_liked'");

                        $PDO->query("UPDATE profile_stats SET xp = xp + 500  WHERE id_user = '$id_src'");
                        $PDO->query("UPDATE profile_stats SET xp = xp + 500  WHERE id_user = '$id_liked'");

                        echo "matched";
                    }
                    else
                        echo "type=relation_state&id_liked=$id_liked";
                }
            }
            else
            {
                $PDO->log_errors("Error in request when updating the database", "false");
                echo "type=set_block&state=false";
            }
        }
        else
        {
            $PDO->log_errors("User already liked", "false");
            echo "already_liked";
        }
    }
    else if ($_POST['state'] === "dislike")
    {
        $tmp_list = explode(',', $list_id_liked);
        if (in_array($id_liked, $tmp_list) != false)
        {
            $tmp_ret = array_diff($tmp_list, [$id_liked]);
            $list_id_liked = implode(',', $tmp_ret);
            if ($PDO->query("UPDATE users SET id_liked = '$list_id_liked' WHERE username = '$user_src'"))
            {
                $container_src = $PDO->query("SELECT id_matchs FROM users WHERE users.id = '$id_src'")->fetchColumn();
                $tmp_container_src = explode(',', $container_src);
                if (in_array($id_liked, $tmp_container_src))
                {
                    $container_dst = $PDO->query("SELECT id_matchs FROM users WHERE users.id = '$id_liked'")->fetchColumn();
                    $tmp_container_dst = explode(',', $container_dst);

                    $tmp_container_src = array_diff($tmp_container_src, [$id_liked]);
                    $tmp_container_dst = array_diff($tmp_container_dst, [$id_src]);



                    $container_src = implode(',', $tmp_container_src);
                    $container_dst = implode(',', $tmp_container_dst);

                    $PDO->query("UPDATE users SET id_matchs = '$container_src' WHERE users.id = '$id_src'");
                    $PDO->query("UPDATE users SET id_matchs = '$container_dst' WHERE users.id = '$id_liked'");

                    echo "unmatched";
                }
                else
                    echo "type=relation_state&id_liked=$id_liked";
            }
            else
            {
                $PDO->log_errors("Error in request when updating the database", "false");
                echo "type=set_block&state=false";
            }
        }
        else
        {
            $PDO->log_errors("User can unliked while is not liked", "false");
            echo "not_liked";
        }
    }
}

else if (isset($_POST['type']) && $_POST['type'] === "user_state")
{
    $user_id   = addslashes($_POST['id_dst']);
    $tmp_seen  = $PDO->query("SELECT last_signin_date FROM users WHERE id = '$user_id'")->fetchColumn();
    $last_seen = strtotime($tmp_seen);

    if (time() > $last_seen + 60)
    {
        $last_seen = date("F j, Y, g:i a", $last_seen);
        echo $last_seen;
    }
    else if (time() < $last_seen + 60)
        echo "online";
}
