<?hh

// Get ip addressof current user with complecated iteration
function get_ip_address()
{
    $ip_keys = array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR');
    foreach ($ip_keys as $key)
    {
        if (array_key_exists($key, $_SERVER) === true)
        {
            foreach (explode(',', $_SERVER[$key]) as $ip)
            {
                // trim for safety measures
                $ip = trim($ip);
                // attempt to validate IP
                if (validate_ip($ip))
                    return $ip;
            }
        }
    }
    return isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : false;
}

function get_json($url)
{
    $timeout = 10;

    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);

    if (preg_match('`^https://`i', $url))
    {
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    }
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; fr; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13');

    $page_content = curl_exec($ch);

    curl_close($ch);

    return $page_content;
}

// Get the location of the current user with a geolocalisation API.
function get_json_location_user($ip)
{
    $url = "http://ip-api.com/json/$ip";
    // $url = "http://freegeoip.net/json/$ip";

    $timeout = 10;

    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);

    if (preg_match('`^https://`i', $url))
    {
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    }
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; fr; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13');

    $page_content = curl_exec($ch);

    curl_close($ch);

    return $page_content;
}

// Function that compute the distance between 4 coordonate points and have
// the possibily to choose th unit system. Return the distance.
function dist($lat1, $lon1, $lat2, $lon2, $unit)
{
    if (empty($lat1) || empty($lon1) || empty($lat2) || empty($lon2))
        return (NULL);
    $radlat1 = M_PI * $lat1/180;
    $radlat2 = M_PI * $lat2/180;
    $radlon1 = M_PI * $lon1/180;
    $radlon2 = M_PI * $lon2/180;

    $theta = $lon1-$lon2;
    $radtheta = M_PI * $theta/180;

    $dist = sin($radlat1) * sin($radlat2) + cos($radlat1) * cos($radlat2) * cos($radtheta);
    $dist = acos($dist);
    $dist = $dist * 180/M_PI;
    $dist = $dist * 60 * 1.1515;

    if ($unit=="K")
    {
        $dist = $dist * 1.609344;
    }

    if ($unit=="N")
    {
        $dist = $dist * 0.8684;
    }
    return $dist;
}
