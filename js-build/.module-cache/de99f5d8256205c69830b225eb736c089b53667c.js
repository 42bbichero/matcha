var React = window.React ? window.React : require('react');

class Auth_form extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            sign_up_username: this.props.sign_up_username,
            sign_up_email: this.props.sign_up_email,
            sign_up_first_name: this.props.sign_up_first_name,
            sign_up_last_name: this.props.sign_up_last_name,
            auth_type: this.props.auth_type,
            reset_form: this.props.reset_form,
        };

        if (this.state.auth_type === "sign_in")
            this.state.auth_type = "sign_in";
        else
            this.state.auth_type = "sign_up";

        this.handleChangeForm = this.handleChangeForm.bind(this);
        this.handleResetForm  = this.handleResetForm.bind(this);
        this.sign_in          = this.sign_in.bind(this);
        this.sign_up          = this.sign_up.bind(this);
        this.reset_pass       = this.reset_pass.bind(this);
        this.reset_key        = this.reset_key.bind(this);
    }


    handleResetForm(event)
    {
        if (this.state.auth_type === "sign_in")
            this.setState({auth_type: "reset_pass"});
        else if (this.state.auth_type === "reset_pass")
            this.setState({auth_type: "sign_in"});
    }
    handleChangeForm(event)
    {
        if (this.state.auth_type === "sign_in")
            this.setState({auth_type: "sign_up"});
        else if (this.state.auth_type === "sign_up")
            this.setState({auth_type: "sign_in"});
    }

    sign_in()
    {
        return (
            React.createElement("div", null, 
                React.createElement("br", null), React.createElement("br", null), 
                React.createElement("img", {src: "../../resources/log.png", alt: "login", height: "50", className: "center"}), 
                React.createElement("br", null), React.createElement("br", null), 
                React.createElement("div", {className: "pure-control-group"}, 
                    React.createElement("input", {className: "sign_in_field", name: "sign_in_username", type: "text", placeholder: "Username", required: true})
                ), 

                React.createElement("div", {className: "pure-control-group"}, 
                    React.createElement("input", {className: "sign_in_field", name: "sign_in_password", type: "password", placeholder: "Password", required: true})
                ), 
                React.createElement("div", {className: "center animated bounceIn"}, 
                    React.createElement("center", null, React.createElement("button", {type: "submit", name: "sign_in", className: "pure-button"}, "Sign in"))
                ), 
                React.createElement("br", null), React.createElement("br", null), 
                React.createElement("div", {className: "center animated bounceIn"}, 
                    React.createElement("center", null, React.createElement("button", {className: "pure-button", onClick: this.handleChangeForm}, "Sign up page"))
                ), 
                React.createElement("br", null), React.createElement("br", null), 
                React.createElement("div", {className: "center animated bounceIn"}, 
                    React.createElement("center", null, React.createElement("button", {className: "pure-button", onClick: this.handleResetForm}, "Forgot password ?"))
                )
            )
        );
    }

    sign_up()
    {
        return (
            React.createElement("div", null, 
                React.createElement("img", {src: "../../resources/log.png", alt: "login", height: "50", className: "center"}), 
                React.createElement("br", null), React.createElement("br", null), 
                React.createElement("div", {className: "pure-control-group"}, 
                    React.createElement("input", {className: "sign_in_field", 
                            name: "sign_up_username", 
                            defaultValue: this.state.sign_up_username, 
                            type: "text", placeholder: "Username", required: true})
                ), 

                React.createElement("div", {className: "pure-control-group"}, 
                    React.createElement("input", {className: "sign_in_field", 
                            name: "sign_up_first_name", 
                            defaultValue: this.state.sign_up_first_name, 
                            type: "text", placeholder: "First Name", required: true})
                ), 

                React.createElement("div", {className: "pure-control-group"}, 
                    React.createElement("input", {className: "sign_in_field", 
                            name: "sign_up_last_name", 
                            defaultValue: this.state.sign_up_last_name, 
                            type: "text", placeholder: "Last Name", required: true})
                ), 

                React.createElement("div", {className: "pure-control-group"}, 
                    React.createElement("input", {className: "sign_in_field", 
                            name: "sign_up_email", 
                            defaultValue: this.state.sign_up_email, 
                            type: "email", placeholder: "Email address", required: true})
                ), 

                React.createElement("div", {className: "pure-control-group"}, 
                    React.createElement("input", {className: "sign_in_field", 
                            name: "sign_up_password", type: "password", placeholder: "Password", required: true})
                ), 

                React.createElement("div", {className: "pure-control-group"}, 
                    React.createElement("input", {className: "sign_in_field", 
                            name: "sign_up_password_confirm", 
                            type: "password", placeholder: "Confirm password", required: true})
                ), 

                React.createElement("div", {className: "pure-controls"}, 
                    React.createElement("input", {name: "sign_up_term", type: "checkbox"}), " ", React.createElement("a", {href: "https://www.youtube.com/embed/Gc2u6AFImn8?rel=0&autoplay=1", target: "_blank"}, "  I ve read the terms and conditions"), 
                    React.createElement("br", null), React.createElement("br", null), 
                    React.createElement("div", {className: "center animated bounceIn"}, 
                        React.createElement("center", null, React.createElement("button", {type: "submit", name: "sign_up", className: "pure-button"}, "Sign up"))
                    )
                ), 
                React.createElement("br", null), React.createElement("br", null), 
                React.createElement("div", {className: "center animated bounceIn"}, 
                    React.createElement("center", null, React.createElement("button", {className: "pure-button", onClick: this.handleChangeForm}, "Sign in page"))
                )
            )
        );
    }

    reset_pass()
    {
        return (
            React.createElement("div", null, 
                React.createElement("img", {src: "../../resources/log.png", alt: "login", height: "50", className: "center"}), 
                React.createElement("br", null), React.createElement("br", null), 
                React.createElement("div", {className: "pure-control-group"}, 
                    React.createElement("input", {className: "sign_in_field", 
                            name: "reset_email", 
                            defaultValue: "", 
                            type: "email", placeholder: "Email address", required: true})
                ), 

                React.createElement("div", {className: "pure-controls"}, 
                    React.createElement("br", null), 
                    React.createElement("div", {className: "center animated bounceIn"}, 
                        React.createElement("center", null, React.createElement("button", {type: "submit", name: "reset", className: "pure-button"}, "Validate"))
                    )
                ), 
                React.createElement("br", null), React.createElement("br", null), 
                React.createElement("div", {className: "center animated bounceIn"}, 
                    React.createElement("center", null, React.createElement("button", {className: "pure-button", onClick: this.handleResetForm}, "Sign in page"))
                )
            )
        );
    }
    reset_key()
    {
        return (
            React.createElement("div", null, React.createElement("form", {action: "/index.php", method: "post"}, 
                React.createElement("img", {src: "../../resources/log.png", alt: "login", height: "50", className: "center"}), 
                React.createElement("br", null), React.createElement("br", null), 
                React.createElement("div", {className: "pure-control-group"}, 
                    React.createElement("input", {className: "sign_in_field", 
                            name: "reset_key_email", 
                            defaultValue: "", 
                            type: "email", placeholder: "Email address", required: true})
                ), 
                React.createElement("div", {className: "pure-control-group"}, 
                    React.createElement("input", {className: "sign_in_field", 
                            name: "reset_password", type: "password", placeholder: "Password", required: true})
                ), 

                React.createElement("div", {className: "pure-control-group"}, 
                    React.createElement("input", {className: "sign_in_field", 
                            name: "reset_password_confirm", 
                            type: "password", placeholder: "Confirm password", required: true})
                ), 
                React.createElement("div", {className: "pure-controls"}, 
                    React.createElement("br", null), 
                    React.createElement("div", {className: "center animated bounceIn"}, 
                        React.createElement("center", null, React.createElement("button", {type: "submit", name: "reset_key", className: "pure-button"}, "Validate"))
                    )
                ), 
                React.createElement("br", null), React.createElement("br", null), 
                React.createElement("div", {className: "center animated bounceIn"}, 
                    React.createElement("center", null, React.createElement("button", {className: "pure-button", onClick: this.handleResetForm}, "Sign in page"))
                ), 
                React.createElement("input", {type: "hidden", name: "q", value: this.state.reset_form})
            ))
        );
    }

    render()
    {
        if (this.state.reset_form)
            return (this.reset_key());
        else if (this.state.auth_type === "sign_in")
            return (this.sign_in());
        else if (this.state.auth_type === "sign_up")
            return (this.sign_up());
        else if (this.state.auth_type === "reset_pass")
            return (this.reset_pass());

    }
}

if (typeof module != 'undefined') {
  module.exports = Auth_form;
} else {
  window.Auth_form = Auth_form;
}
