<?php

/*
* Connection class for exchange between site and DB
*/
class Connection
{
	private $log_request;
	private $debug_req;
	private $username;
	private $id_user;

	// PDO object for execute request
	private static $PDO;

	// connection to the database
	public function __construct($hostname, $db_name, $username, $password, $log_request, $debug_req, $option = NULL)
	{
		try
		{
			self::$PDO = new PDO('mysql:host=' . $hostname . ';dbname=' . $db_name . ";charset=utf8",
			$username,
			$password,
			$option);
		}
		catch (Exception $e)
		{
			echo "Can't connect to mysql<br>";
			trigger_error($e->getMessage(), E_USER_ERROR);
		}
		$this->log_request = $log_request;
		$this->debug_req = $debug_req;
		$this->username = "dev-matcha";

		return (self::$PDO);
	}

	public function getIdUser()
	{
		return $this->id_user;
	}

	public function setIdUser($id_user)
	{
		if (is_numeric($id_user) && (int)$id_user > 0)
			$this->id_user = (int)$id_user;
		else
			$this->id_user = 0;
	}

	public function setUsername($username)
	{
		if (!empty($username))
			$this->username = $username;
	}

	public function log_errors($message, $status)
	{
		$message = addslashes($message);
		if ($status === "ini" || $status === "false")
			$this->query("INSERT INTO log_errors (username, message, server_uri, creation_date, status)
						VALUES ('" . $this->username . "', '$message', 'server_uri', now(), '$status')");
	}

	public function prepare($request, $params)
	{
		// Check return of execute()
		if (($req = self::$PDO->prepare($request)) == FALSE)
		{
			$this->log_request(self::$PDO, $request, "FALSE");
			exit;
		}
		//	self::$PDO->query("INSERT INTO log_request_wr (username, request, server_uri, creation_date, status)
		//						VALUES ('$this->username', '', 'server_uri', NOW(), 'false')");
		try
		{
			$this->log_request($request, "INI");
			$req->execute($params);
		}
		catch (Exception $e)
		{
			$this->log_request($request, "FALSE");
			trigger_error($e->getMessage(), E_USER_ERROR);
			exit;
		}
		return ($req);
	}

	// Execute or query the request and return the result of the request + the last_insert_id
	public function req_last_insert($request_option, $request, $params = NULL)
	{
		// Escape criticals char
		//$request = $this->check($request);
		if ($request_option === "query")
		{
			if (($result = self::$PDO->query($request)) == FALSE)
			{
				echo "Error in the request : $request<br>";
				$this->log_request($request, "FALSE");
				exit;
			}
		}
		else if ($request_option === "prepare")
		{
			$req = self::$PDO->prepare($request);
			if (($req->execute($params)) == FALSE)
			{
				echo "Error in the request : $request<br>";
				$this->log_request($request, "FALSE");
				exit;
			}
		}

		$last_id = self::$PDO->lastInsertId();
		return ($last_id);
	}

	// to execute a request to the database and log the request in a sql table
	public function query($request)
	{
		try
		{
			$this->log_request($request, "INI");
			$result = self::$PDO->query($request);
		}
		catch (Exception $e)
		{
			$this->log_request($request, "FALSE");
			trigger_error($e->getMessage(), E_USER_ERROR);
			exit;
		}

		return $result;
	}

	//Get back the request to write it in the DB
	public function log_request($request, $status)
	{
		if ($this->debug_req == 1)
			echo "$request<br>"; //Print the $sql (to debug)

		if ($this->log_request === "SW")
		{
			if (!strstr($request, "INSERT INTO") && !strstr($request, "UPDATE") && !strstr($request, "DELETE") && !strstr($request, "DROP"))
			{
				try
				{
					$request = addslashes($request);
					$sql = "INSERT INTO log_request_sh (username, request, server_uri, creation_date, status) VALUES (:username, :request, :server_uri, now(), :status)";
					$params = array(
						"username" => $this->username,
						"request" => $request,
						"server_uri" => 'server_uri',
						"status" => $status
					);
					$req = self::$PDO->prepare($sql);
					$req->execute($params);
				}
				catch (Exception $e)
				{
					trigger_error($e->getMessage(), E_USER_ERROR);
				}
			}
		}

		if ($this->log_request === "RW" || $this->log_request === "SW")
		{
			// for log INSERT INTO, UPDATE, DELETE and DROP
			if (strstr($request, "INSERT INTO") || strstr($request, "UPDATE") || strstr($request, "DELETE") || strstr($request, "DROP"))
			{
				try
				{
					$request = addslashes($request);
					$sql = "INSERT INTO log_request_wr (username, request, server_uri, creation_date, status) VALUES (:username, :request, :server_uri, now(), :status)";
					$params = array(
						"username" => $this->username,
						"request" => $request,
						"server_uri" => 'server_uri',
						"status" => $status
					);
					$req = self::$PDO->prepare($sql);
					$req->execute($params);
				}
				catch (Exception $e)
				{
					trigger_error($e->getMessage(), E_USER_ERROR);
				}
			}
		}
	}
}
