<?hh

// Set the hash function.
ini_set('session.hash_function', "sha512");
// How many bits per character of the hash.
// The possible values are '4' (0-9, a-f), '5' (0-9, a-v), and '6' (0-9, a-z, A-Z, "-", ",").
ini_set('session.hash_bits_per_character', 5);
// Force the session to only use cookies, not URL variables.
ini_set('session.use_only_cookies', 1);
// Disabled display of session ID
ini_set('session.use_trans_sid', false);

// Don't allow uninitialized session_id
ini_set("session.use_strict_mode", 1);

// Start session
session_start();

// Include header
require("core/config.php");
require(__DIR__ . "/vendor/autoload.php");
require(__DIR__ . "/vendor/facebook/xhp-lib/init.php");

$log_out_header =
"<!DOCTYPE html>
<html>".
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <!-- CSS includes -->
        <link rel="stylesheet" type="text/css" href="/css/animate.css" />
        <link rel="stylesheet" type="text/css" href="/css/styles.css" />
        <link rel="stylesheet" type="text/css" href="/css/styles.css" />
        <link rel="stylesheet" type="text/css" href="/css/main_page.css" />
        <link rel="stylesheet" href="https://unpkg.com/purecss@0.6.1/build/pure-min.css" />

        <!-- JS bundle include -->
        <script src="/js-build/bundle.js"></script>
        <title>Matcha</title>
    </head>;

// Print log out header
echo $log_out_header;

// Check if user is auth
if (isset($_SESSION['auth']['username']) && isset($_SESSION['auth']['session_id']))
{
    session_destroy();
    unset($_SESSION);

    echo
    "<body class='animated slideInLeft'>
        <center><h2 class='animated slideInLeft' style='color: #fff'>You have been successfully logged out.</h2><br><br>
        <a style='color: #fff' href=\"$url_path/\" name=\"connection\">Connection</a></center>
    </body>";
}
else
{
    echo
    "<body class='animated slideInLeft'>
        <center><h2 class='animated slideInLeft' style='color: #fff'>Please log in before log out.</h2><br><br>
        <center><a style='color: #fff' href=\"$url_path/\" name=\"connection\">Connection</a></center>
    </body>";
}
